var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    clean = require('gulp-clean'),
    imagemin = require('gulp-tinypng'),
    jsmin = require("gulp-uglify"),
    jade = require("gulp-jade"),
    babel= require("gulp-babel"),
    babelify = require('babelify'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream');

/**Babel task**/
gulp.task("babel", function () {
        browserify({
            entries: './src/js/components/comments.jsx',
            extensions: ['.jsx'],
            debug: true
        })
        .transform('babelify',{presets: ['es2015', 'react']})
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(gulp.dest("./dist/components/"));
});

///*Clean TASK*/
gulp.task('clean', function(){
    return gulp.src("./dist/css/*.css")
        .pipe(clean())
        .pipe(gulp.dest('./dist/css/'));
});

///*POST CSS TASK*/
gulp.task('sass-dev',['clean'], function () {
    return gulp.src("./src/scss/*.scss")
        //.pipe(sass({outputStyle: 'compressed'}))
        //.pipe(sourcemaps.init())
        //.pipe(sass().on("error",sass.logError))
        //.pipe(sourcemaps.write())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(gulp.dest('./dist/css/'));


});

//IMAGEMIN
gulp.task('image', function () {
    gulp.src('./src/images/**/*.{jpg,png}')
        .pipe(imagemin('7yD9n5WhOsyqSh0ufJ-EopmOfriY40q3'))
        .pipe(gulp.dest('./dist/images/'));
});

///*COPY FONTS*/
gulp.task('copy-fonts', function(){
    gulp.src('src/fonts/**/*.{ttf,woff,woff2,eot,svg}')
        .pipe(gulp.dest('dist/fonts/'));
});

///*JADE TASK*/
gulp.task('jade', function() {
    return gulp.src("./src/jade/**/*.jade")
        .pipe(jade({pretty: true}))
        .pipe(gulp.dest('./dist/html/'))
});

///*JS-MIN TASK*/
gulp.task("js-min", function() {
    return gulp.src("./src/js/*.js")
        .pipe(jsmin())
        .pipe(gulp.dest("./dist/js/"))
});

///*WATCH TASK*/
gulp.task("watch", function (){
    gulp.watch('./src/scss/**/**/*.scss',   ['sass-dev']);
    gulp.watch('./src/jade/**/*.jade',   ['jade']);
    gulp.watch('./src/js/**/*.js',       ['js-min']);
    //gulp.watch('./src/js/components/*.jsx', ['babel']);
});


gulp.task("default", ['sass-dev', 'jade','copy-fonts','image',"js-min"], function(){});
gulp.task("default-jade", ['jade'], function(){});
gulp.task("default-sass", ['sass'], function(){});
gulp.task("default-image",['image'], function(){});
gulp.task("default js-min",['js-min'], function(){});

gulp.task("default-babel", ['babel'], function(){});